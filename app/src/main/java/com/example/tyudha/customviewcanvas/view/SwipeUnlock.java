package com.example.tyudha.customviewcanvas.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.tyudha.customviewcanvas.R;
import com.example.tyudha.customviewcanvas.view.listener.PositionSwipeListener;

/**
 * Created by tyudha on 1/26/18.
 */

public class SwipeUnlock extends RelativeLayout {

    private ImageView imageSlide;
    private RelativeLayout baseLayout;

    private PositionSwipeListener positionSwipeListener;

    public SwipeUnlock(Context context) {
        super(context);
        initLayout(context);
    }

    public SwipeUnlock(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public SwipeUnlock(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context);
    }

    private void initLayout(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_swipe_unlock,  this);
        declareView(view);
        onDragLogic();
    }

    private void declareView(View view) {
        baseLayout = view.findViewById(R.id.swipeunlock_base_relativelayout);
        imageSlide = view.findViewById(R.id.swipeunlock_image_imageview);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void onDragLogic() {
        imageSlide.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                float xS = motionEvent.getRawX();
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        break;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        if(xS < getWidthScreen() + convertDpToPixel(imageSlide.getWidth(), getContext()) && xS > 0) {
                            setSliderAndProcess(xS - convertDpToPixel(imageSlide.getWidth(), getContext()));
                        }
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        if(xS < getWidthScreen() / 2) {
                            setSliderAndProcess(0);
                        } else if(xS >= getWidthScreen() / 2) {
                            setSliderAndProcess(getWidthScreen() - imageSlide.getWidth());
                        }
                        checkIfScrollIsOnLeftSide();
                        break;
                    }
                    case MotionEvent.ACTION_OUTSIDE: {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    private void setSliderAndProcess(float positionX) {
        imageSlide.setX(positionX);
        positionSwipeListener.setProgress((int) measureProgressInPercent(positionX));
    }

    private float measureProgressInPercent(float nowLocation) {
        float percentageProcess = (nowLocation / (baseLayout.getWidth() - imageSlide.getWidth())) * 100;

        if(percentageProcess >= 0 && percentageProcess <= 100) {
            return percentageProcess;
        } else if(percentageProcess > 100) {
            return 100;
        } else {
            return 0;
        }
    }

    private void checkIfScrollIsOnLeftSide() {
        if(imageSlide.getX() == 0) {
            positionSwipeListener.setOnFalsePositionListener();
        } else if(imageSlide.getX() == getWidthScreen() - imageSlide.getWidth()) {
            positionSwipeListener.setOnTruePositionListener();
        }
    }

    private float getWidthScreen() {
        return baseLayout.getWidth();
    }

    private static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public void setOnPositionListener(PositionSwipeListener positionListener) {
        this.positionSwipeListener = positionListener;
    }
}
