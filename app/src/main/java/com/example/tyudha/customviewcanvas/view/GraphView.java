package com.example.tyudha.customviewcanvas.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by tyudha on 1/29/18.
 */

public class GraphView extends View {

    public GraphView(Context context) {
        super(context);
    }

    public GraphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public GraphView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(10);

        drawBaseLine(canvas, paint);
        drawHeightBar(canvas, paint);
    }

    private void drawBaseLine(Canvas canvas, Paint paint) {
        canvas.drawLine(100, getScreenHeight() - 220, getScreenWidth() - 100, getScreenHeight() - 220, paint);
    }

    private void drawHeightBar(Canvas canvas, Paint paint) {
        for (int a = getScreenHeight() - 220; a > 220; a--) {
            canvas.drawLine(120, getScreenHeight() - 220, 120, a, paint);
        }

        invalidate();
    }

    private int getScreenWidth() {
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    private int getScreenHeight() {
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        return displayMetrics.heightPixels;
    }
}
