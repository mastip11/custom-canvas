package com.example.tyudha.customviewcanvas.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tyudha.customviewcanvas.R;
import com.example.tyudha.customviewcanvas.view.SwipeUnlock;
import com.example.tyudha.customviewcanvas.view.listener.PositionSwipeListener;

public class SwipeActivity extends AppCompatActivity {

    private SwipeUnlock swipeUnlock;
    private TextView progressText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe);

        swipeUnlock = findViewById(R.id.main_swipeunlock);
        progressText = findViewById(R.id.main_progress);

        swipeUnlock.setOnPositionListener(new PositionSwipeListener() {
            @Override
            public void setOnFalsePositionListener() {
                Toast.makeText(SwipeActivity.this, "Left", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void setOnTruePositionListener() {
                Toast.makeText(SwipeActivity.this, "Right", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void setProgress(int progress) {
                progressText.setText("Progress : " + progress + "%");
            }
        });
    }
}
