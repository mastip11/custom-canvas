package com.example.tyudha.customviewcanvas.view.listener;

/**
 * Created by tyudha on 1/29/18.
 */

public interface PositionSwipeListener {
    void setOnFalsePositionListener();
    void setOnTruePositionListener();
    void setProgress(int progress);
}
